from app.exts import app
from app.exts import settings

flask_conf = settings['application']['flask']
config = settings['application']['config']
app.config.update(**config)

if __name__ == "__main__":
    app.run(**flask_conf)
'''
pip freeze --all > requirements.txt
gunicorn run:app
'''
