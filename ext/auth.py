from typing import List
from functools import wraps
from flask import current_app


class HTTPAuth(object):

    @classmethod
    def auth(cls, authentication_classes: List):
        def set_auth_internal(func):
            @wraps(func)
            def decorated(*args, **kwargs):
                if authentication_classes:
                    for i in authentication_classes:
                        authenticate = i().authenticate
                        authenticate()
                return cls.ensure_sync(func)(*args, **kwargs)

            return decorated

        return set_auth_internal

    @staticmethod
    def ensure_sync(func):
        try:
            return current_app.ensure_sync(func)
        except AttributeError:
            return func
