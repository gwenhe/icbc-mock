import os
import sys
import yaml


def read_yaml(file):
    with open(file, encoding="utf-8") as f:
        yaml_data = yaml.load(f.read(), Loader=yaml.FullLoader)
    return yaml_data


def conf(env, setting_url):
    file_path = os.path.join(sys.path[0], setting_url, (env + '.yaml'))
    settings = read_yaml(file_path)
    return settings
