import os
from flask import Flask
from ext.setting import conf

""" 设置配置文件 """
os.environ['FLASK_ENV'] = 'prod'
env = os.getenv('FLASK_ENV')
settings = conf(env=env, setting_url='config')

""" 组件 """
app = Flask(__name__)
