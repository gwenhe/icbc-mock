import qrcode
import io
import base64
import random
import string
from flask import Blueprint, request
import redis
from app.schemas.efq_schemas import RedisGetSchema
from app.uitls.exceptions import APIException
from app.uitls.auth import HTTPTokenAuth
from app.uitls.code import ResponseCode

efq = Blueprint('efq', __name__, url_prefix='/efq')


def url_image(url):
    img = qrcode.make(url)
    img = img.get_image()
    in_mem_file = io.BytesIO()
    img.save(in_mem_file, format="PNG")
    in_mem_file.seek(0)
    img_bytes = in_mem_file.read()
    base64_encoded_result_bytes = base64.b64encode(img_bytes)
    base64_encoded_result_str = base64_encoded_result_bytes.decode('ascii')
    return base64_encoded_result_str


@efq.route('mnpesvr/inner/getqrcode', methods=['POST'])
def get_qrcode():
    """
    获取二维码
    :return:
    """
    org_no = request.values.get('orgno')
    sales_no = request.values.get('salesno')
    base_url = 'http://192.168.0.224:19080/h5/home/business?orgno={}&salesno={}&wetoken={}'
    ran_str = ''.join(random.sample(string.ascii_letters + string.digits, 24))
    we_token = 'wetoken-' + ran_str
    applet_url = base_url.format(org_no, sales_no, we_token)
    base64_img_url = url_image(applet_url)
    res_data = {
        'errcode': 0,
        'qrcode': base64_img_url,
    }
    return res_data


@efq.route('/mnpesvr/inner/getopenid', methods=['POST'])
def get_open_id():
    """
    获取openid
    :return:
    """
    token = request.values.get('token')
    res_data = {
        'errcode': 0,
        'openid': token.split('wetoken-')[1]
    }
    return res_data


@efq.route('/gdapi/aam/verify', methods=['POST'])
def verify():
    """
    e分期登录 身份验证接口
    :return:
    """
    return {
        'retcode': '0000',
        'retmsg': '成功',
    }


def get_redis(res_dict):
    key = res_dict['key']
    del res_dict['key']
    r = redis.Redis(**res_dict)
    data = r.get(key)
    if data:
        return data.decode()
    else:
        return data


@efq.route('/redis/get', methods=['POST'])
def redis_get():
    """
    获取redis字符串
    MeterSphere无法连接redis，提供给姜婷婷使用
    :return:
    """
    load_schema = RedisGetSchema()
    res_dict = load_schema.load(request.json)
    data = get_redis(res_dict)
    return {'value': data}
