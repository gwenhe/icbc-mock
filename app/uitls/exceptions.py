from app.exts import app
from app.uitls.response import APIResponse
from app.uitls.code import ResponseCode, ExceptionCode, CodeMessage, ResponseMessage
from marshmallow import ValidationError


class APIException(Exception):

    def __init__(self, code: int, message=None):
        self.code = code
        self.message = message

    def __str__(self):
        return str(self.message)


def get_code(error_code):
    switcher = {
        ExceptionCode.TOKEN_INVALID: CodeMessage.TOKEN_INVALID,
    }
    code_msg = switcher.get(error_code, None)
    if code_msg is None:
        raise ValueError
    return code_msg


@app.errorhandler(Exception)
def all_exception_handler(e):
    if isinstance(e, APIException):
        print(e.code)
        code_msg = get_code(e.code)
        return APIResponse(code_msg=code_msg)
    if isinstance(e, ValidationError):
        print(e.messages)
        return APIResponse(msg=e.messages, code=ResponseCode.FAIL)
    else:
        app.logger.exception(e)
        return APIResponse(code=ResponseCode.FAIL, msg=ResponseMessage.ERROR)
