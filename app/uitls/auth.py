# -*- coding: utf-8 -*-
import json
from flask import request
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from app.uitls.code import ExceptionCode
from app.uitls.exceptions import APIException


class HTTPTokenAuth(object):
    HEADER = 'Authorization'
    SCHEME = 'Bearer'
    SECRET_KEY = '-#1=4hj(0c1^5rd1=tg8-+wc9n*!o=h2*pd@bc(x9#t$54oc34'
    PRESCRIPTION = 1200
    REDIS_TOKEN = 'token:'

    # def create_token(self, payload, user_info, expiration=3600):
    #     s = Serializer(self.SECRET_KEY, expires_in=expiration)
    #     token = s.dumps(payload).decode()
    #     redis_token = self.REDIS_TOKEN + token
    #     user_info = json.dumps(user_info, ensure_ascii=False)
    #     redis.set(redis_token, user_info, ex=self.PRESCRIPTION)
    #     return token

    def get_token(self):
        try:
            scheme, token = request.headers.get(self.HEADER, '').split(None, 1)
            if scheme == self.SCHEME:
                return token
        except ValueError:
            return None

    def get_user_info(self):
        pass

    # def authenticate(self):
    #     """
    #     校验登录鉴权
    #     :return:
    #     """
    #
    #     token = self.get_token()
    #     redis_token = self.REDIS_TOKEN + token
    #     token_ttl = redis.ttl(redis_token)
    #
    #     if token is None or token_ttl < 0:
    #         raise APIException(ExceptionCode.TOKEN_INVALID)
    #     redis.expire(redis_token, self.PRESCRIPTION)
