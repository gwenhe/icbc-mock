# -*- coding: utf-8 -*-
import json
import decimal
from typing import Dict
from flask import Response
from app.uitls.code import ResponseCode, ResponseMessage


class DecimalEncoder(json.JSONEncoder):
    """
    decimal编码器，解决json模块无法序列化decimal对象问题
    """

    def default(self, obj):
        if isinstance(obj, decimal.Decimal):
            return float(obj)


class APIResponse(Response):
    # 格式化data
    def __init__(self, code=ResponseCode.SUCCESS, msg=ResponseMessage.SUCCESS, code_msg: Dict = None, data=None,
                 status=200, mimetype='application/json', headers: Dict = None):
        if code_msg is None:
            self.res_data = {
                'code': code,
                'msg': msg
            }
        else:
            self.res_data = code_msg
        if data:
            self.res_data['data'] = data

        # 序列化
        res_data = json.dumps(self.res_data, ensure_ascii=False, cls=DecimalEncoder)

        # 处理完毕数据后，剩下的过程还是丢给Response类中的init做完
        super(APIResponse, self).__init__(
            response=res_data, status=status, mimetype=mimetype, headers=headers)
