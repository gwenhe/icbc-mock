from marshmallow import Schema, fields, ValidationError
import functools
from marshmallow.fields import Field
from marshmallow.validate import (
    URL, Email, Range, Length, Equal, Regexp,
    Predicate, NoneOf, OneOf, ContainsOnly
)

URL.default_message = '无效的链接'
Email.default_message = '无效的邮箱地址'
Range.message_min = '不能小于{min}'
Range.message_max = '不能小于{max}'
Range.message_all = '不能超过{min}和{max}这个范围'
Length.message_min = '长度不得小于{min}位'
Length.message_max = '长度不得大于{max}位'
Length.message_all = '长度不能超过{min}和{max}这个范围'
Length.message_equal = '长度必须等于{equal}位'
Equal.default_message = '必须等于{other}'
Regexp.default_message = '非法输入'
Predicate.default_message = '非法输入'
NoneOf.default_message = '非法输入'
OneOf.default_message = '无效的选择'
ContainsOnly.default_message = '一个或多个无效的选择'
Field.default_error_messages = {
    'required': '该字段是必填字段',
    'type': '无效的输入类型',
    'null': '字段不能为空',
    'validator_failed': '无效的值'
}


class BaseSchema(Schema):
    class Meta:
        # 固定排序
        ordered = True


class PaginateSchema(BaseSchema):
    has_next = fields.Bool()
    has_prev = fields.Bool()
    next_num = fields.Integer()
    prev_num = fields.Integer()
    pages = fields.Integer()
    total = fields.Integer()
