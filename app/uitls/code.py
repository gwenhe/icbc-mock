# -*- coding: utf-8 -*-
class Code(object):

    @classmethod
    def set_code(cls, code, message):
        """
        TODO(GuoWenHe): 待完善
        """
        return {
            'code': code,
            'message': message
        }


class ResponseCode:
    SUCCESS = 0  # 成功
    FAIL = -1  # 失败，前端弹框提示msg
    TOKEN_INVALID = 4001


class ResponseMessage:
    SUCCESS = "success"
    ERROR = '系统异常'


class CodeMessage:
    TOKEN_INVALID = Code.set_code(4001, '登录无效或已过期')


class ExceptionCode(Code):
    TOKEN_INVALID = 4001
