from marshmallow import Schema, fields, ValidationError
from app.uitls.base_schemas import BaseSchema


class RedisGetSchema(BaseSchema):
    host = fields.String(required=True)
    port = fields.Integer(required=True)
    db = fields.Integer(required=True)
    password = fields.String()
    key = fields.String(required=True)
