"""
配置文档: https://docs.gunicorn.org/en/latest/settings.html#
"""
proc_name = 'icbc-mock'
bind = '0.0.0.0:5000'  # 绑定ip和端口号
worker_class = 'gevent'
workers = 1
threads = 2
timeout = 60
backlog = 2048
worker_connections = 1000
daemon = True
